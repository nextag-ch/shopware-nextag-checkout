import Vue from "../../node_modules/vue/dist/vue";
import axios from "../../node_modules/axios/dist/axios";
import DatePicker  from 'vue2-datepicker'
import '../../node_modules/vue2-datepicker/index.css'
import '../../node_modules/vue2-datepicker/locale/fr';
import '../../node_modules/vue2-datepicker/locale/de';

(function () {
  "use strict";
  NextCheckout.ViewModel.MainObject = Vue.extend({
    mounted: function () {
      this.loadData()
    },
    data: function () {
      return {
        data: {},
        config: {},
        isLoading: true,
        commission: null,
        comment: null,
        deliverydate: null,
        disabledBefore: null,
        disabledAfter: null
      };
    },
    components: { DatePicker },
    computed: {},
    watch: {},
    methods: {      
      formattedDate(dateString) {
        if (dateString && dateString != "") {
          const d = new Date(dateString);
          const ye = new Intl.DateTimeFormat(this.config.languageShort, { year: 'numeric' }).format(d)
          const mo = new Intl.DateTimeFormat(this.config.languageShort, { month: 'short' }).format(d)
          const da = new Intl.DateTimeFormat(this.config.languageShort, { day: '2-digit' }).format(d)
          return `${da}. ${mo} ${ye}`;
        }
        return dateString;
      },
      loadData() {        
        console.log("todo load checkout data logic if needed")        
        const today = new Date()
        const startDate = new Date()
        const endDate = new Date()
        startDate.setDate(today.getDate() + 1)
        endDate.setDate(today.getDate() + 15)
        this.disabledBefore = startDate
        this.disabledAfter = endDate
      },
    },
  });
})();
