<?php

namespace Nextag\Checkout\Content;

use Shopware\Core\Checkout\Cart\Event\CheckoutOrderPlacedEvent;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;

class CheckoutService
{
    private $checkoutRepository;

    public function __construct(EntityRepositoryInterface $repository)
    {
        $this->checkoutRepository = $repository;
    }

    public function save($data, CheckoutOrderPlacedEvent $event)
    {
        if (!empty($data)) {
            $data["orderId"] = $event->getOrder()->getId();
            $this->checkoutRepository->create([
                $data
            ], $event->getContext());
        }
    }
}
